
A small collection of semi-trivial scripts.

### hpkp-hash

This wrapper around openssl takes a TLS certificate and generates a
HPKP (RFC 7469) formated hash. The script is inspired by an example in
the [About Public Key Pinning][1] blog post.

    $ hpkp-hash gandi-intermediate.crt

### isodl

A helper script to securely download new Debian alt. Ubuntu isos. For
the gpg verification it relies on keyrings already installed by the
operating system.

    $ isodl http://ftp.se.debian.org/debian-cd/8.1.0/amd64/iso-cd/debian-8.1.0-amd64-netinst.iso


### maybe

For those situations where */bin/true* and */bin/false* are too
predictive one can instead use *maybe*.

    # maybe || reboot

### vrt-template-prep

There are a few things which needs to be done to a template image
before it can be used to create new virtual machines. That is what
this script helps out with.

    root@template:~# vrt-template-prep
    Template image can now be shutdown.
    Next boot will bring fresh ssh host keys and a clean bash history.
    root@template:~#


[1]: https://noncombatant.org/2015/05/01/about-http-public-key-pinning/

